<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Asuransi_svg-icon svg-fill</name>
   <tag></tag>
   <elementGuidId>47ede5d7-6654-4351-8e61-88884a71bf14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bl-dropdown__icon-toggle > svg.svg-icon.svg-fill</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/following::*[name()='svg'][2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item bl-product-list-wrapper&quot;]/div[@class=&quot;bl-flex-container direction-column&quot;]/div[2]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-flex-container mb-24 align-items-center&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-dropdown te-product-sorting mr-16 bl-dropdown--has-value&quot;]/div[@class=&quot;bl-dropdown__box&quot;]/div[@class=&quot;bl-dropdown__icon-toggle&quot;]/svg[@class=&quot;svg-icon svg-fill&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>5b327f79-af71-435b-ab6c-66089964f94c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>version</name>
      <type>Main</type>
      <value>1.1</value>
      <webElementGuid>74346b3f-a5d8-4e6e-8989-129beead2bbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
      <webElementGuid>03399e0f-80d1-49e1-8198-c5cb71965080</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-icon svg-fill</value>
      <webElementGuid>bb6b9fa6-ccfb-4dd4-8483-b52723ab1ba2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item bl-product-list-wrapper&quot;]/div[@class=&quot;bl-flex-container direction-column&quot;]/div[2]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-flex-container mb-24 align-items-center&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-dropdown te-product-sorting mr-16 bl-dropdown--has-value&quot;]/div[@class=&quot;bl-dropdown__box&quot;]/div[@class=&quot;bl-dropdown__icon-toggle&quot;]/svg[@class=&quot;svg-icon svg-fill&quot;]</value>
      <webElementGuid>6b954704-5226-498c-a7aa-578a924f65c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/following::*[name()='svg'][2]</value>
      <webElementGuid>95c2aa09-23c5-4d62-a7df-9fc93393b9a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[2]/following::*[name()='svg'][12]</value>
      <webElementGuid>7926f977-d365-41f9-90e5-9432a16c8cea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laptop HP 11-f104TU Celeron n2840 Ram 2GB Ssd 120GB 11.6 inch'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>7e33aaf1-9269-42df-a823-278198664daf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[3]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>7cecd48d-49a0-471f-8c87-1304a72bee4f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
