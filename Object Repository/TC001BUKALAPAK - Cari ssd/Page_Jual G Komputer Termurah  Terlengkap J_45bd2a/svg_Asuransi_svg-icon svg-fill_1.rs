<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Asuransi_svg-icon svg-fill_1</name>
   <tag></tag>
   <elementGuidId>b95367f4-ac04-4f3f-95bf-c7c06bf23b1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bl-dropdown__icon-toggle > svg.svg-icon.svg-fill</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/following::*[name()='svg'][2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>844d4f3d-7650-455c-87f0-fd2010a83c1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>version</name>
      <type>Main</type>
      <value>1.1</value>
      <webElementGuid>95f81d51-eb1b-4880-aab4-249aff84e6d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
      <webElementGuid>62c7845d-e81e-4ac8-b7a1-b51b82f33ba7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-icon svg-fill</value>
      <webElementGuid>f1247103-665d-4066-8528-c85fb2fcbd36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item bl-product-list-wrapper&quot;]/div[@class=&quot;bl-flex-container direction-column&quot;]/div[2]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-flex-container mb-24 align-items-center&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-dropdown te-product-sorting mr-16 bl-dropdown--active bl-dropdown--has-value&quot;]/div[@class=&quot;bl-dropdown__box&quot;]/div[@class=&quot;bl-dropdown__icon-toggle&quot;]/svg[@class=&quot;svg-icon svg-fill&quot;]</value>
      <webElementGuid>2116e4ed-740f-49f8-952a-21c6459c6cff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/following::*[name()='svg'][2]</value>
      <webElementGuid>f5c55ebc-3e1e-48a9-a6c7-33e816a36024</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[2]/following::*[name()='svg'][12]</value>
      <webElementGuid>ed5cf1eb-fbe4-4682-a3b8-9588d804be19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laptop HP 11-f104TU Celeron n2840 Ram 2GB Ssd 120GB 11.6 inch'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>dbc3137a-b7d4-4ba1-8681-e11022edea11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[3]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>883aa6bb-15e6-4adf-b4bc-bcb07ba9213c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
