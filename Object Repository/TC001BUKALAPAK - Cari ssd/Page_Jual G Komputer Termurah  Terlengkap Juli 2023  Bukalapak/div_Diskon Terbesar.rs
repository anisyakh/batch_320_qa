<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Diskon Terbesar</name>
   <tag></tag>
   <elementGuidId>e7b4f482-d102-4071-b905-79a02c33a46d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='product-explorer-container']/div/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/ul/li[7]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3984f8aa-4f05-4408-8696-4bb05947f45f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bl-dropdown__option__item-wrapper</value>
      <webElementGuid>32bd97fe-44c5-433c-b2c4-51949c209b83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> 
              Diskon Terbesar
             </value>
      <webElementGuid>10efcbd1-2d29-40be-a038-469661a52ad8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item bl-product-list-wrapper&quot;]/div[@class=&quot;bl-flex-container direction-column&quot;]/div[2]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-flex-container mb-24 align-items-center&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-dropdown te-product-sorting mr-16 bl-dropdown--active bl-dropdown--has-value&quot;]/div[@class=&quot;bl-dropdown__content&quot;]/ul[@class=&quot;bl-dropdown__option&quot;]/li[@class=&quot;bl-dropdown__option__item&quot;]/div[@class=&quot;bl-dropdown__option__item-wrapper&quot;]</value>
      <webElementGuid>8f1087f7-4ce5-48cd-9a04-d5534702c050</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='product-explorer-container']/div/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/ul/li[7]/div</value>
      <webElementGuid>c72ecd6a-40e8-4640-bd04-17a83572fdea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/following::div[25]</value>
      <webElementGuid>15cea6ee-2417-4d56-b155-1f2edbd3f832</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[2]/following::div[52]</value>
      <webElementGuid>0e282e1d-d0d3-4fd4-85e9-f6f8da455cbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laptop HP 11-f104TU Celeron n2840 Ram 2GB Ssd 120GB 11.6 inch'])[1]/preceding::div[10]</value>
      <webElementGuid>a242807b-43cd-444a-98e5-a7565f993531</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[3]/preceding::div[11]</value>
      <webElementGuid>7a767918-976a-4c90-87a4-918fa22dd610</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/div</value>
      <webElementGuid>9d51d98c-f743-4f12-8cbb-965a549cf730</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' 
              Diskon Terbesar
             ' or . = ' 
              Diskon Terbesar
             ')]</value>
      <webElementGuid>d26929f0-4621-49fa-a496-0465a5d27d72</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
