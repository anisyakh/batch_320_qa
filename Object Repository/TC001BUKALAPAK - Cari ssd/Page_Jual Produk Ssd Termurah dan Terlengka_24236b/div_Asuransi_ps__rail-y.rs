<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Asuransi_ps__rail-y</name>
   <tag></tag>
   <elementGuidId>0c6aed3d-0fa9-4ecb-af44-7503e4b77a25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ps__rail-y</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='product-explorer-container']/div/div/div/div/div[2]/div[10]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bcb6991f-710b-4ce9-804b-1121299cf19d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ps__rail-y</value>
      <webElementGuid>3073ad3b-ace6-4459-8692-41aa95f100f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-sidebar-filter&quot;]/div[@class=&quot;sidebar-scroll-area ps ps--active-y ps--scrolling-y&quot;]/div[@class=&quot;ps__rail-y&quot;]</value>
      <webElementGuid>136c425a-7e7f-4e97-b110-0e8040143ba7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='product-explorer-container']/div/div/div/div/div[2]/div[10]</value>
      <webElementGuid>5d16e319-90ac-4926-9cf7-09e0def5483e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/following::div[3]</value>
      <webElementGuid>cbb83531-578e-4956-84ce-e2e92c1588be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[2]/following::div[30]</value>
      <webElementGuid>57e07598-f2f9-4c2b-b608-a6afa2e3a793</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hapus Semua'])[1]/preceding::div[7]</value>
      <webElementGuid>f6d5b303-d15f-49e7-be31-5d2a79f60a23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pc Core i7 ram 16Gb SSD'])[1]/preceding::div[24]</value>
      <webElementGuid>a48e495f-c93b-49a9-b36f-ae6d39a9ded3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[10]</value>
      <webElementGuid>48d7d4d2-a56a-4106-bfad-1d43a0b88420</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
