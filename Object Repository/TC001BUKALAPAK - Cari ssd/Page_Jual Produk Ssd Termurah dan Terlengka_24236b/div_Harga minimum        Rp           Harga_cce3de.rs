<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Harga minimum        Rp           Harga_cce3de</name>
   <tag></tag>
   <elementGuidId>2144a1e2-7171-4d5c-9990-d4076d666cb0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.te-filter-price</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='product-explorer-container']/div/div/div/div/div[2]/div[4]/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c7212ed1-1a33-472f-9918-73330a4d16fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>te-filter-price</value>
      <webElementGuid>039b81ad-c30b-4520-bd93-6ab119fc6cd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Harga minimum  
      Rp
           Harga maksimum  
      Rp
          </value>
      <webElementGuid>4b5051e4-0f3e-490c-aeba-018b4597b89d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-sidebar-filter&quot;]/div[@class=&quot;sidebar-scroll-area ps ps--active-y ps--scrolling-y&quot;]/div[@class=&quot;bl-accordion is-active&quot;]/div[@class=&quot;bl-accordion__body&quot;]/div[@class=&quot;pb-8&quot;]/div[1]/div[@class=&quot;te-filter-price&quot;]</value>
      <webElementGuid>23150211-e6ae-4547-be1f-57e996c1e73f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='product-explorer-container']/div/div/div/div/div[2]/div[4]/div[2]/div/div/div</value>
      <webElementGuid>17e15c21-f0ce-4db5-a839-33761c9c5bbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat lebih banyak'])[2]/following::div[8]</value>
      <webElementGuid>6d52cb44-f36f-4b86-ae78-2f9983f56fa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Paxel Same Day'])[1]/following::div[8]</value>
      <webElementGuid>1a4cef50-8d19-40ca-a121-b7f0578ab080</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div/div</value>
      <webElementGuid>d206b115-457d-428f-b8f0-665c7ef43649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Harga minimum  
      Rp
           Harga maksimum  
      Rp
          ' or . = 'Harga minimum  
      Rp
           Harga maksimum  
      Rp
          ')]</value>
      <webElementGuid>24452c06-2ec2-4106-9a3a-fb5c12750917</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
