<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Bekas_svg-icon svg-fill</name>
   <tag></tag>
   <elementGuidId>22294f1a-e5c9-42c6-8347-787bd7554859</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-sidebar-filter&quot;]/div[@class=&quot;sidebar-scroll-area ps ps--active-y&quot;]/div[@class=&quot;bl-accordion is-active&quot;]/div[@class=&quot;bl-accordion__head bl-accordion__head--top-padding bl-accordion__head--bottom-padding&quot;]/div[@class=&quot;bl-accordion__icon&quot;]/svg[@class=&quot;svg-icon svg-fill&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[2]/following::*[name()='svg'][2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>a1690dd6-1c31-4ab3-9b95-c2fb1e61acbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>version</name>
      <type>Main</type>
      <value>1.1</value>
      <webElementGuid>830a94cb-5f11-4c1d-b1c4-6b1d9b0fb737</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
      <webElementGuid>f038469e-750d-4486-9c18-ce71d71fdaeb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-icon svg-fill</value>
      <webElementGuid>87565af5-1d03-436e-b2b1-a5aba4fb17f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-explorer-container&quot;)/div[@class=&quot;bl-container&quot;]/div[@class=&quot;bl-flex-container&quot;]/div[@class=&quot;bl-flex-item&quot;]/div[@class=&quot;bl-sidebar-filter&quot;]/div[@class=&quot;sidebar-scroll-area ps ps--active-y&quot;]/div[@class=&quot;bl-accordion is-active&quot;]/div[@class=&quot;bl-accordion__head bl-accordion__head--top-padding bl-accordion__head--bottom-padding&quot;]/div[@class=&quot;bl-accordion__icon&quot;]/svg[@class=&quot;svg-icon svg-fill&quot;]</value>
      <webElementGuid>26fe0304-35c3-4fe9-abdc-e49a678ff0f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bekas'])[2]/following::*[name()='svg'][2]</value>
      <webElementGuid>c3e2216f-8a54-44c3-a768-7a10c5bb70fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Baru'])[1]/following::*[name()='svg'][4]</value>
      <webElementGuid>81d9c5fa-068a-4792-91ba-479bee88d476</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asuransi'])[1]/preceding::*[name()='svg'][9]</value>
      <webElementGuid>40e36033-d57a-4050-9795-8d49b6ce575a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hapus Semua'])[1]/preceding::*[name()='svg'][11]</value>
      <webElementGuid>d7029b2f-68a9-4699-8b28-843ea4d6baeb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
