<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Harga Tertinggi</name>
   <tag></tag>
   <elementGuidId>1c618dc7-389e-4d45-8b2c-a7f2ca923ca7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::p[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>85030edc-74d0-4678-bcc5-539f69266790</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Typography</value>
      <webElementGuid>30c8b2b8-09a9-4df0-a398-1d8e076fb402</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-3tkidh-unf-heading e1qvo2ff8</value>
      <webElementGuid>bcbcb873-4df0-4e32-ace6-c31bfd9557d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Harga Tertinggi</value>
      <webElementGuid>60f0c11c-4758-4633-ade4-94ccaec1e3c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[6]/div[@class=&quot;css-u9rfd2 e17suhym1&quot;]/div[@class=&quot;css-19os3da e83okfj4&quot;]/ul[@class=&quot;css-rxgulu&quot;]/li[@class=&quot;css-d2qf6r e83okfj5&quot;]/button[@class=&quot;css-nzq9o0&quot;]/div[@class=&quot;css-18biwo&quot;]/div[1]/p[@class=&quot;css-3tkidh-unf-heading e1qvo2ff8&quot;]</value>
      <webElementGuid>cc002c9b-b0d9-4d4c-a7e8-e4e733c3bf66</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::p[4]</value>
      <webElementGuid>f87929f6-1d75-4514-b1a3-ae6a772830a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::p[4]</value>
      <webElementGuid>1892cb58-b8d1-468b-b2be-72999be1bc86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Harga Tertinggi']/parent::*</value>
      <webElementGuid>71f34d40-558d-4c9e-a9a3-ede04440acd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/button/div/div/p</value>
      <webElementGuid>7e2e5f92-7f10-43c1-9846-126ffdf9da92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Harga Tertinggi' or . = 'Harga Tertinggi')]</value>
      <webElementGuid>c7c440db-c397-4891-8c7e-ddd450c52a2a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
