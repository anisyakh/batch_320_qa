<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Urutkan_unf-icon css-1w4dubj</name>
   <tag></tag>
   <elementGuidId>5d14c46b-55b2-45e8-a0a8-f78e6aa20360</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg.unf-icon.css-1w4dubj</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Urutkan:'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>4da673e9-caac-4344-ab67-a8ed6a8308f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>unf-icon css-1w4dubj</value>
      <webElementGuid>9d570ac8-5291-43c2-87b3-23accdeb82ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>ac616d64-4ff9-4c84-8113-40a4a81eeb00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>86fb6da7-7113-4176-9ea5-4c3c2b23b833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>84a2ba2e-bed6-474d-8427-279c23fd39b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>var(--color-icon-enabled, #2E3137)</value>
      <webElementGuid>0a5853e8-c11b-495b-a452-37ef41d06276</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-jau1bt&quot;]/div[@class=&quot;css-1c82svt&quot;]/div[@class=&quot;css-rjanld&quot;]/div[@class=&quot;css-k1aaub&quot;]/div[@class=&quot;css-16hrxnz&quot;]/div[@class=&quot;css-1ro2jof e83okfj0&quot;]/button[@class=&quot;css-1g467vj e83okfj1&quot;]/svg[@class=&quot;unf-icon css-1w4dubj&quot;]</value>
      <webElementGuid>c9a4f84d-7da0-4f76-b05e-88887ac2edc1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Urutkan:'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>6070f844-badc-499c-b488-31e3fd274a22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('', '&quot;', 'ps5', '&quot;', '')])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>5a83d170-ba86-4f11-88cf-94e73d1336de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dipromosikan oleh'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>9e464617-640f-4a15-9b90-9e2933d3872d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
