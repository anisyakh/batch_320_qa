<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Harga Tertinggi</name>
   <tag></tag>
   <elementGuidId>868acf53-305f-487a-9b3b-94eb839a6475</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Harga Tertinggi' or . = 'Harga Tertinggi')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::button[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.css-1p9nymi.e83okfj5 > button.css-nzq9o0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4cef500a-c48f-4c73-8ba7-c43b260fbd5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-nzq9o0</value>
      <webElementGuid>62f4cac2-5e74-47a0-9960-ed9a8bc8925e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unf</name>
      <type>Main</type>
      <value>select-menu-item-btn</value>
      <webElementGuid>f5e019c4-a0e6-4e81-a928-1a776c00e9f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-has-subitem</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>06214cb1-ba6c-4135-92a9-ea86a3528afa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-item-text</name>
      <type>Main</type>
      <value>Harga Tertinggi</value>
      <webElementGuid>90a05aba-4df1-4cd6-8c90-734083956b8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Harga Tertinggi</value>
      <webElementGuid>fe8fa898-0924-4198-8d02-886d958a7472</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[7]/div[@class=&quot;css-146iqb e17suhym1&quot;]/div[@class=&quot;css-19os3da e83okfj4&quot;]/ul[@class=&quot;css-rxgulu&quot;]/li[@class=&quot;css-1p9nymi e83okfj5&quot;]/button[@class=&quot;css-nzq9o0&quot;]</value>
      <webElementGuid>1f8c89db-f781-4d1a-a64c-91a8dcb77a10</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::button[4]</value>
      <webElementGuid>eb892c7a-b779-417b-bce1-3939905bd293</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::button[4]</value>
      <webElementGuid>af756fa9-2298-4ec5-8e28-663fb49d244e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/button</value>
      <webElementGuid>84cc14df-bd02-43bf-b888-21392c53a557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Harga Tertinggi' or . = 'Harga Tertinggi')]</value>
      <webElementGuid>912601b9-6e7f-4090-98b9-64ad8960b4c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
