<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Harga Terendah</name>
   <tag></tag>
   <elementGuidId>3bb1cdbd-6f5d-47ae-b6b2-aa9071133a15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Harga Terendah' or . = 'Harga Terendah')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = 'Harga Terendah' or . = 'Harga Terendah')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>22b03130-42e2-419a-bce3-fb21e6f4752c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Typography</value>
      <webElementGuid>00d64c3c-9c41-42b6-800d-fa1c7389b018</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-3tkidh-unf-heading e1qvo2ff8</value>
      <webElementGuid>ec08ce23-2868-451d-ba5b-79323f135179</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Harga Terendah</value>
      <webElementGuid>d1795292-c7a8-4731-b0ff-fc60faa5eec2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/div[@class=&quot;css-u9rfd2 e17suhym1&quot;]/div[@class=&quot;css-19os3da e83okfj4&quot;]/ul[@class=&quot;css-rxgulu&quot;]/li[@class=&quot;css-d2qf6r e83okfj5&quot;]/button[@class=&quot;css-nzq9o0&quot;]/div[@class=&quot;css-18biwo&quot;]/div[1]/p[@class=&quot;css-3tkidh-unf-heading e1qvo2ff8&quot;]</value>
      <webElementGuid>3f79e258-c520-4d25-9220-7f093043c9d2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Harga Terendah' or . = 'Harga Terendah')]</value>
      <webElementGuid>99e85098-5c9d-42eb-b3ea-330101472e3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
