<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Official Store</name>
   <tag></tag>
   <elementGuidId>f5c48f94-03a8-481a-b86a-724e149b5b52</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-frokek</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='zeus-root']/div/div[2]/div/div/div/div/div/div/div[2]/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>57fe2f28-306a-4234-bcd7-268e6e5ce9e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-frokek</value>
      <webElementGuid>621d4b87-ecc1-47a9-b3eb-9c3545e719b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>dSRPShopFilter</value>
      <webElementGuid>116c80aa-eb1e-44a2-99dc-d0923ba43cd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Official Store</value>
      <webElementGuid>c4e5efb0-2866-4277-b75b-9f07276009e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-jau1bt&quot;]/div[@class=&quot;css-1c82svt&quot;]/div[@class=&quot;css-gfb3wv&quot;]/div[@class=&quot;css-18nstr8&quot;]/div[1]/div[@class=&quot;css-khi8sg&quot;]/div[1]/div[@class=&quot;css-1ghv7i5&quot;]/div[@class=&quot;css-1m93f3h&quot;]/div[1]/div[@class=&quot;css-1cb34wj&quot;]/div[@class=&quot;css-frokek&quot;]</value>
      <webElementGuid>7a699add-4427-4189-98b5-0b8b16bb986d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-root']/div/div[2]/div/div/div/div/div/div/div[2]/div/div/div/div</value>
      <webElementGuid>3f8117e7-7dd1-4cea-826a-fa53ddc748ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis toko'])[1]/following::div[4]</value>
      <webElementGuid>bb2f0b9d-b6d0-4bf1-bef0-11db950b53ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wearable Devices'])[1]/following::div[5]</value>
      <webElementGuid>c94973f1-d372-456d-8100-5229e8ae7b8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Power Merchant Pro'])[1]/preceding::div[7]</value>
      <webElementGuid>84fcb46e-6d90-4e78-a3a4-d1031c644d71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div[2]/div/div/div/div</value>
      <webElementGuid>7dbf6d14-a389-41fd-bb44-14346a319890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Official Store' or . = 'Official Store')]</value>
      <webElementGuid>fe28486b-6c81-4d53-bb91-72c4c1ae5822</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
