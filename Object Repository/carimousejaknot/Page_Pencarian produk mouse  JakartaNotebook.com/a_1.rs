<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_1</name>
   <tag></tag>
   <elementGuidId>ac588bef-2f2c-478d-a194-ea981575a955</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.isActive</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>938693d7-f525-4729-8c32-944d7ec3b159</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.jakartanotebook.com/search?key=mouse&amp;show=60&amp;sort=lowprice&amp;page=1</value>
      <webElementGuid>7ab22427-380e-4f2e-b81e-4332357a9e2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>isActive</value>
      <webElementGuid>14bcd39c-f974-4aff-80c8-59e13350bf9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>f6f32dd4-42c4-4faa-b1d6-2e28fec4003c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>b2b2c8d4-614a-4c52-a17f-dd1d9ca477ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy no-touch history rgba multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent&quot;]/body[@class=&quot;page-body&quot;]/div[1]/div[@class=&quot;mainContent&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;paging box&quot;]/ul[1]/li[@class=&quot;page&quot;]/a[@class=&quot;isActive&quot;]</value>
      <webElementGuid>8e9d590a-943d-4d29-9a19-2beb42027861</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'1')]</value>
      <webElementGuid>b36ab284-06b4-4b25-9662-28ba609c9ce4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::a[1]</value>
      <webElementGuid>bb07b1e9-5606-46e8-b92f-025afc526b01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stock Filter'])[1]/following::a[2]</value>
      <webElementGuid>d26fb34d-7234-4331-bbb3-4b878fc977b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OMAN0NBK'])[1]/preceding::a[4]</value>
      <webElementGuid>b241cfb5-6368-4c53-986c-05396f1126b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='1']/parent::*</value>
      <webElementGuid>3acc5b2f-a38e-4b9a-bcc7-aea60ecc1890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.jakartanotebook.com/search?key=mouse&amp;show=60&amp;sort=lowprice&amp;page=1')]</value>
      <webElementGuid>bce28159-cd06-494f-9b82-a1f56ec837e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>654ebded-4548-4a70-a5d3-136bcdce94c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.jakartanotebook.com/search?key=mouse&amp;show=60&amp;sort=lowprice&amp;page=1' and @title = '1' and (text() = '1' or . = '1')]</value>
      <webElementGuid>1e4646ff-74fd-4c34-aec2-ff80a4648c0b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
