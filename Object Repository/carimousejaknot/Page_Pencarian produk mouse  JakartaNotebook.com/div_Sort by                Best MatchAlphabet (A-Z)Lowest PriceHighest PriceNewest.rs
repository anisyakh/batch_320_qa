<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sort by                Best MatchAlphabet (A-Z)Lowest PriceHighest PriceNewest</name>
   <tag></tag>
   <elementGuidId>a35c6a65-fb5e-4014-aa13-98506885b54f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.categorySorting.sortBy</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-list-item']/div/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ae209cd0-0d2c-40a8-8d73-bbefb9f7490f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>categorySorting  sortBy</value>
      <webElementGuid>267a4c31-ed06-4ad3-a134-a568891aed00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Sort by
                
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                
            </value>
      <webElementGuid>740f370f-a932-4df1-aec8-fb67e76b6902</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-list-item&quot;)/div[@class=&quot;categoryHeader&quot;]/div[@class=&quot;categorySorting  sortBy&quot;]</value>
      <webElementGuid>42a34bd9-51b8-4f23-afb2-28b3096e0d50</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-list-item']/div/div[3]</value>
      <webElementGuid>227b725e-41fc-4f02-9d9e-35908fe25ab8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show'])[1]/following::div[1]</value>
      <webElementGuid>22b49e92-ea45-415a-9c8a-6de4391dccfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Stock mouse'])[1]/following::div[2]</value>
      <webElementGuid>49ad514c-e195-416a-a0b7-66ab66ca7ed6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stock Filter'])[1]/preceding::div[1]</value>
      <webElementGuid>1c243f3c-9c2e-4de6-b3b5-064ac52705de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div[3]</value>
      <webElementGuid>9a61dce2-d7f9-4e20-bd5b-92e5cd5e212a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Sort by
                
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                
            ' or . = '
                Sort by
                
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                
            ')]</value>
      <webElementGuid>b607b3e8-3344-4934-980d-30493b7aadd9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
