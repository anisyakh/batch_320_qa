<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Best MatchAlphabet (A-Z)Lowest PriceHighest PriceNewest</name>
   <tag></tag>
   <elementGuidId>a1e7f7dd-8f1e-44c9-b704-8a9759a0b225</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;sort&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'sort' and (text() = '
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                ' or . = '
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='sort']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>222923b9-eaac-469b-987c-8dd2411fc99f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>sort</value>
      <webElementGuid>2829d932-23f1-46b0-8691-6e762a90442d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>filterSelect</value>
      <webElementGuid>1c897cde-0dc6-478f-bfbb-3c859d982d1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                </value>
      <webElementGuid>8d156c43-80a5-4c47-963d-4479a583ff6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-list-item&quot;)/div[@class=&quot;categoryHeader&quot;]/div[@class=&quot;categorySorting  sortBy&quot;]/select[@class=&quot;filterSelect&quot;]</value>
      <webElementGuid>e977df6e-d3bb-407b-8503-86939afe36de</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='sort']</value>
      <webElementGuid>f8ea7dd8-a7c5-4922-90c7-d8d51f202099</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-list-item']/div/div[3]/select</value>
      <webElementGuid>4df2f22c-8037-40ef-bd60-668a51c5dacc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[1]/following::select[1]</value>
      <webElementGuid>90437684-0e60-4c36-8385-42696e0982b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show'])[1]/following::select[2]</value>
      <webElementGuid>8f6e4df2-0af3-49a5-822d-6a85d9232609</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stock Filter'])[1]/preceding::select[1]</value>
      <webElementGuid>b937cff8-6052-461c-b58d-b4b01f4c042f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/preceding::select[2]</value>
      <webElementGuid>df6bdd20-e340-4d1f-bac5-1f870c927a14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/select</value>
      <webElementGuid>81107740-492a-4338-891e-9668178f7646</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'sort' and (text() = '
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                ' or . = '
			Best Match
			Alphabet (A-Z)
			Lowest Price
			Highest Price
			Newest
                ')]</value>
      <webElementGuid>a5b88a48-cbdc-4fa1-8353-07bd8d8f45f3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
