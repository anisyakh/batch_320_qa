import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.bukalapak.com/')

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Situs Belanja Online dan Jual Beli Mud_3c8940/div_BukaBantuan_v-omnisearch'))

WebUI.setText(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Situs Belanja Online dan Jual Beli Mud_3c8940/input_BukaBantuan_searchkeywords'), 
    produk)

WebUI.sendKeys(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Situs Belanja Online dan Jual Beli Mud_3c8940/input_BukaBantuan_searchkeywords'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap J_45bd2a/svg_Asuransi_svg-icon svg-fill'))

if (sort_by == 'Terbaru') {
    WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap J_45bd2a/div_Terbaru'))
} else if (sort_by == 'Termurah') {
    WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap J_45bd2a/div_Termurah'))
} else if (sort_by == 'Terlaris') {
    WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap J_45bd2a/div_Terlaris'))
} else if (sort_by == 'Diskon Terbesar') {
    WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap J_45bd2a/div_Diskon Terbesar'))
}

WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/html_Jual Produk Ssd Termurah dan Terlengka_a46243'))

WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/div_Asuransi_ps__rail-y'))

if (kondisi == 'Baru') {
    WebUI.scrollToElement(findTestObject('TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap Juli 2023  Bukalapak/span_Baru'), 
        5)

    WebUI.click(findTestObject('TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/span_Baru'))
} else if (kondisi == 'Bekas') {
    WebUI.scrollToElement(findTestObject('TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap Juli 2023  Bukalapak/span_Bekas'), 
        5)

    WebUI.click(findTestObject('TC001BUKALAPAK - Cari ssd/Page_Jual G Komputer Termurah  Terlengkap Juli 2023  Bukalapak/span_Bekas'))
}

WebUI.waitForPageLoad(5)

WebUI.scrollToElement(findTestObject('TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/div_Harga minimum        Rp           Harga_cce3de'), 
    5)

WebUI.click(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/div_Harga minimum        Rp           Harga_cce3de'))

if (harga_minimum == '500000') {
    WebUI.setText(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/input_Rp_bl-text-field__input'), 
        '500000')
} else if (harga_minimum == '100000') {
    WebUI.setText(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/input_Rp_bl-text-field__input'), 
        '100000')
} else if (harga_minimum == '750000') {
    WebUI.setText(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/input_Rp_bl-text-field__input'), 
        '750000')
} else if (harga_minimum == '1000000') {
    WebUI.setText(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/input_Rp_bl-text-field__input'), 
        '1000000')
}

WebUI.sendKeys(findTestObject('Object Repository/TC001BUKALAPAK - Cari ssd/Page_Jual Produk Ssd Termurah dan Terlengka_24236b/input_Rp_bl-text-field__input'), 
    Keys.chord(Keys.ENTER))

WebUI.closeBrowser()

