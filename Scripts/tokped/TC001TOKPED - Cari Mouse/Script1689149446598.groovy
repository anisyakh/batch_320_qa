import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.tokopedia.com/')

WebUI.waitForPageLoad(5)

WebUI.setText(findTestObject('Object Repository/TC001TOKPED - Cari Mouse/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_Kategori_css-3017qm exxxdg63'), 
    'casio gshock')

WebUI.sendKeys(findTestObject('Object Repository/TC001TOKPED - Cari Mouse/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_Kategori_css-3017qm exxxdg63'), 
    Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('TC001TOKPED - Cari Mouse/Page_Jual casio gshock  Tokopedia/svg_Urutkan_unf-icon css-1w4dubj'))

WebUI.click(findTestObject('TC001TOKPED - Cari Mouse/Page_Jual casio gshock  Tokopedia/button_Harga Terendah'))

WebUI.waitForPageLoad(5)

WebUI.scrollToPosition(1115, 345)

WebUI.click(findTestObject('Object Repository/TC001TOKPED - Cari Mouse/Page_Jual casio gshock  Tokopedia/div_Official Store'))

WebUI.waitForPageLoad(5)

WebUI.setText(findTestObject('Object Repository/TC001TOKPED - Cari Mouse/Page_Jual casio gshock  Tokopedia/input_Rp_pmin'), 
    '1000000')

WebUI.sendKeys(findTestObject('Object Repository/TC001TOKPED - Cari Mouse/Page_Jual casio gshock  Tokopedia/input_Rp_pmin'), 
    Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/TC001TOKPED - Cari Mouse/Page_Jual casio gshock  Tokopedia/div_Baru'))

WebUI.closeBrowser()

