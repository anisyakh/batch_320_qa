import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.jakartanotebook.com/')

WebUI.waitForPageLoad(3)

WebUI.setText(findTestObject('Object Repository/carimousejaknot/Page_JakartaNotebook  Toko Online Lengkap  _55a6a0/input_Kategori_key'), 
    produk)

WebUI.sendKeys(findTestObject('Object Repository/carimousejaknot/Page_JakartaNotebook  Toko Online Lengkap  _55a6a0/input_Kategori_key'), 
    Keys.chord(Keys.ENTER))

WebUI.waitForPageLoad(3)

if (show == '40') {
    WebUI.selectOptionByValue(findTestObject('Object Repository/carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/select_4060100'), 
        '40', false)
} else if (show == '60') {
	WebUI.selectOptionByValue(findTestObject('Object Repository/carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/select_4060100'),
		'60', false)	
} else if (show == '100') {
    WebUI.selectOptionByValue(findTestObject('Object Repository/carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/select_4060100'), 
        '100', false)
}

WebUI.waitForPageLoad(3)

if (sort_by == 'lowprice') {
    WebUI.selectOptionByValue(findTestObject('carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/select_Best MatchAlphabet (A-Z)Lowest PriceHighest PriceNewest'), 
        'lowprice', false)
} else if (sort_by == 'highprice') {
    WebUI.selectOptionByValue(findTestObject('carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/select_Best MatchAlphabet (A-Z)Lowest PriceHighest PriceNewest'), 
        'highprice', false)
}

WebUI.waitForPageLoad(3)

if (page == '2') {
    WebUI.click(findTestObject('Object Repository/carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/a_2'))
} else if (page == '3') {
    WebUI.click(findTestObject('Object Repository/carimousejaknot/Page_Pencarian produk mouse  JakartaNotebook.com/a_3'))
}

WebUI.closeBrowser()

